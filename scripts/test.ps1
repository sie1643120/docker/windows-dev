#
# Copyright (c) 2017 Marat Abrarov (abrarov@gmail.com)
#
# Distributed under the MIT License (see accompanying LICENSE)
#

# Stop immediately if any error happens
$ErrorActionPreference = "Stop"


# Fonction pour récupérer la version à partir des tags Git
function Get-VersionFromGit {
  # Exécuter la commande Git pour obtenir les tags
  # $tags = git tag --list "V-*" | Sort-Object -Descending
  $tags = git describe --tags  --abbrev=0  --match "V-*"
  # Vérifier si des tags existent
  if ($tags -and $tags -match "^V-(\d+\.\d+\.\d+)$") {
      # Extraire la version du dernier tag
      $version = $matches[1]
  } else {
      # Version par défaut si aucun tag n'est trouvé
      $version = "0.0.0"
  }

  return $version
}


try {
  $project_dir = (Get-Item "${PSScriptRoot}").Parent.FullName
  $image_version = "dev"
  $image_repository = "${env:CI_REGISTRY_IMAGE}:${image_version}"

  Write-Host "Running 7-Zip in container created from ${image_repository} image"
  docker run --rm "${image_repository}" "C:\Program Files\7-Zip\7z.exe" i
  if (${LastExitCode} -ne 0) {
    throw "Failed to get help from 7-Zip command line utility"
  }

  # Write-Host "Running Git in container created from ${image_repository} image"
  # docker run --rm "${image_repository}" git --version
  # if (${LastExitCode} -ne 0) {
  #   throw "Failed to get version of Git"
  # }

  # Write-Host "Running CMake in container created from ${image_repository} image"
  # docker run --rm "${image_repository}" "C:\cmake\bin\cmake.exe" --version
  # if (${LastExitCode} -ne 0) {
  #   throw "Failed to get version of CMake"
  # }

}
catch {
    Write-Error $_.Exception.Message
    exit 1
}

exit 0
