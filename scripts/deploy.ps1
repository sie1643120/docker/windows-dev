#
# Copyright (c) 2017 Marat Abrarov (abrarov@gmail.com)
#
# Distributed under the MIT License (see accompanying LICENSE)
#

# Stop immediately if any error happens
$ErrorActionPreference = "Stop"


# Fonction pour récupérer la version à partir des tags Git
function Get-VersionFromGit {
  # Exécuter la commande Git pour obtenir les tags
  # $tags = git tag --list "V-*" | Sort-Object -Descending
  $tags = git describe --tags  --abbrev=0  --match "V-*"
  # Vérifier si des tags existent
  if ($tags -and $tags -match "^V-(\d+\.\d+\.\d+)$") {
      # Extraire la version du dernier tag
      $version = $matches[1]
  } else {
      # Version par défaut si aucun tag n'est trouvé
      $version = "0.0.0"
  }

  return $version
}


try {
  $project_dir = (Get-Item "${PSScriptRoot}").Parent.FullName
  $image_version = Get-VersionFromGit
  $image_repository = "${env:CI_REGISTRY_IMAGE}:${image_version}"

  Write-Host "Pull dev image ${env:CI_REGISTRY_IMAGE}:dev"
  docker pull "${env:CI_REGISTRY_IMAGE}:dev"
  if (${LastExitCode} -ne 0) {
    throw "Failed to pull ${env:CI_REGISTRY_IMAGE}:dev image"
  }

  Write-Host "Tagging ${env:CI_REGISTRY_IMAGE}:dev image as ${image_repository}"
  docker tag "${env:CI_REGISTRY_IMAGE}:dev" "${image_repository}"
  if (${LastExitCode} -ne 0) {
    throw "Failed to tag ${env:CI_REGISTRY_IMAGE}:dev image as ${image_repository}"
  }

  Write-Host "Tagging ${env:CI_REGISTRY_IMAG}:${image_version} image as latest"
  docker tag "${image_repository}" "${env:CI_REGISTRY_IMAGE}:latest"
  if (${LastExitCode} -ne 0) {
    throw "Failed to tag ${image_repository} image as ${env:CI_REGISTRY_IMAGE}:latest"
  }

  Write-Host "Detected version of ${image_repository}"

  Write-Host "Pushing ${image_repository} image"
  docker push "${image_repository}"
  if (${LastExitCode} -ne 0) {
    throw "Failed to push ${image_repository} image"
  }

  Write-Host "Pushing ${env:CI_REGISTRY_IMAGE}:latest image"
  docker push "${env:CI_REGISTRY_IMAGE}:latest"
  if (${LastExitCode} -ne 0) {
    throw "Failed to push ${env:CI_REGISTRY_IMAGE}:latest image"
  }

  Write-Host "Delet dev tag from reposotery."
  
  curl --header "PRIVATE-TOKEN: ${env:CI_REGISTRY_TOKEN}" --request DELETE "https://gitlab.com/api/v4/projects/${env:CI_PROJECT_ID}/registry/repositories/${REGISTRY_REPOSITORY_ID}/tags/${TAG_NAME}"'  
  $headers = @{ "PRIVATE-TOKEN" = "${env:CI_REGISTRY_TOKEN}" }
  $url = "https://gitlab.com/api/v4/projects/${env:CI_PROJECT_ID}/registry/repositories/${REGISTRY_REPOSITORY_ID}/tags/env"
  Write-Output "URL: $url"
  # try {
  #   $response = Invoke-RestMethod -Uri $url -Method Delete -Headers $headers
  #   Write-Output "Tag deleted successfully."
  # } catch {
  #   Write-Output "Error deleting tag: $_"
  # }
  # $headers = @{ "PRIVATE-TOKEN" = "${env:CI_REGISTRY_TOKEN}" }
  # $url = "https://gitlab.com/api/v4/projects/${env:CI_PROJECT_ID}/registry/repositories/${env:REGISTRY_REPOSITORY_ID}/tags/dev"
  # Invoke-RestMethod -Uri $url -Method Delete -Headers $headers


}
catch {
    Write-Error $_.Exception.Message
    exit 1
}

exit 0
