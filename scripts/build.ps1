#
# Copyright (c) 2017 Marat Abrarov (abrarov@gmail.com)
#
# Distributed under the MIT License (see accompanying LICENSE)
#

# Stop immediately if any error happens
$ErrorActionPreference = "Stop"


# Fonction pour récupérer la version à partir des tags Git
function Get-VersionFromGit {
  # Exécuter la commande Git pour obtenir les tags
  # $tags = git tag --list "V-*" | Sort-Object -Descending
  $tags = git describe --tags  --abbrev=0  --match "V-*"
  # Vérifier si des tags existent
  if ($tags -and $tags -match "^V-(\d+\.\d+\.\d+)$") {
      # Extraire la version du dernier tag
      $version = $matches[1]
  } else {
      # Version par défaut si aucun tag n'est trouvé
      $version = "0.0.0"
  }

  return $version
}


try {
  $project_dir = (Get-Item "${PSScriptRoot}").Parent.FullName
  #TODO: find way to deal with tags and versions
  $image_version = "dev"
  $image_revision = "$(git -C "${project_dir}" rev-parse --verify HEAD)"
  $image_repository = "${env:CI_REGISTRY_IMAGE}:${image_version}"

  Write-Host "Building ${env:CI_REGISTRY_IMAG}:${image_version} image with ${image_revision} revision"
  docker build `
    -t "${image_repository}" `
    --build-arg "image_version=${image_version}" `
    --build-arg "image_revision=${image_revision}" `
    "${project_dir}\src"
  if (${LastExitCode} -ne 0) {
    throw "Failed to build ${env:CI_REGISTRY_IMAG} image"
  }

  # docker save -o .\dockerimage.tar $image_repository 
  # if (${LastExitCode} -ne 0) {
  #   throw "Failed to save ${image_repository} image"
  # }




  docker push "${image_repository}"
  if (${LastExitCode} -ne 0) {
    throw "Failed to push ${image_repository} image"
  }
  # Write-Host "Tagging ${env:CI_REGISTRY_IMAG}:${image_version} image as latest"
  # docker tag "${env:CI_REGISTRY_IMAG}:${image_version}" "${env:CI_REGISTRY_IMAG}:latest"
  # if (${LastExitCode} -ne 0) {
  #   throw "Failed to tag ${env:CI_REGISTRY_IMAG}:${image_version} image as ${env:CI_REGISTRY_IMAG}:latest"
  # }
}
catch {
    Write-Error $_.Exception.Message
    exit 1
}

exit 0
