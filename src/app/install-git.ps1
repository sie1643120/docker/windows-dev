function InstallGit {
    $git_folder = "${env:DOWNLOAD_DIR}/git"

    # Obtenir tous les fichiers .msi dans le dossier
    $exeFiles = Get-ChildItem -Path "${git_folder}" -Filter *-64-bit.exe
    # Si aucun fichier .exe n'est trouvé, arrêter le script
    if ($exeFiles.Count -eq 0) {
        throw "No Install of Cmake found in download folder."
    }
    
    $git_upper_version = $exeFiles | Sort-Object { Get-VersionFromFilename $_.Name } -Descending | Select-Object -First 1
    $git_dist = "${git_folder}/${git_upper_version}"

    Write-Host "Installing git from ${git_dist} into ${env:GIT_HOME}"
    $p = Start-Process -FilePath "${git_dist}" `
        -ArgumentList ("/VERYSILENT", "/SUPPRESSMSGBOXES", "/ALLUSERS", "/NORESTART", "DIR ${env:GIT_HOME}") `
        -Wait -PassThru
    if (${p}.ExitCode -ne 0) {
        throw "Failed to install git"
    }
    Write-Host "Git ${git_upper_version} installed into ${env:GIT_HOME}"
}