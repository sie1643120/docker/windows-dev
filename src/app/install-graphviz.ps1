function InstallGraphviz {
    # Install Graphviz
    $graphviz_folder = "${env:DOWNLOAD_DIR}/graphviz"

    # Obtenir tous les fichiers .msi dans le dossier
    $exeFiles = Get-ChildItem -Path "${graphviz_folder}" -Filter *win64.exe

    # Si aucun fichier .exe n'est trouvé, arrêter le script
    if ($exeFiles.Count -eq 0) {
        throw "No Install of Graphviz found in download folder."
    }

    # Trouver le fichier avec la version la plus récente

    $graphviz_upper_version = $exeFiles | Sort-Object { Get-VersionFromFilename $_.Name } -Descending | Select-Object -First 1
    $graphviz_dist = "${graphviz_folder}/${graphviz_upper_version}"

    Write-Host "Installing Graphviz from ${graphviz_dist} into ${env:GRAPHVIZ_HOME}"
    $p = Start-Process -FilePath "${graphviz_dist}" `
        -ArgumentList ("/S", "/D ""${env:GRAPHVIZ_HOME}""") `
        -Wait -PassThru
    if (${p}.ExitCode -ne 0) {
        throw "Failed to install Graphviz"
    }
    Write-Host "Graphviz ${graphviz_upper_version} installed into ${env:GRAPHVIZ_HOME}"


}