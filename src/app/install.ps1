Import-Module -Name "${PSScriptRoot}/tools.ps1"
Import-Module -Name "${PSScriptRoot}/install-7z.ps1"
Import-Module -Name "${PSScriptRoot}/install-cmake.ps1"
Import-Module -Name "${PSScriptRoot}/install-git.ps1"
Import-Module -Name "${PSScriptRoot}/install-doxygen.ps1"
# Import-Module -Name "${PSScriptRoot}/install-java.ps1"
Import-Module -Name "${PSScriptRoot}/install-graphviz.ps1"
Import-Module -Name "${PSScriptRoot}/install-plantuml.ps1"

# Stop immediately if any error happens
$ErrorActionPreference = "Stop"

# Enable all versions of TLS
[System.Net.ServicePointManager]::SecurityProtocol = @("Tls12", "Tls11", "Tls", "Ssl3")


# Install 7-zip
Install7zip

# Install cmake
# InstallCmake

# Install git
# InstallGit

# Install doxygen
# InstallDoxygen

# install JAVA
# InstallJava

# Install Graphviz
# InstallGraphviz

# Install Plantuml
# InstallPlantuml

#Ajout du dossier dans le path de windows
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";${env:SEVEN_ZIP_HOME};${env:CMAKE_HOME}\bin;${env:GIT_HOME}\bin;${env:DOXYGEN_HOME}\bin;${env:JAVA_HOME}\bin;${env:GRAPHVIZ_HOME}\bin;${env:PLANTUML_HOME}", [EnvironmentVariableTarget]::Machine)