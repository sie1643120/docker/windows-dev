function InstallPlantuml{
    $plantuml_folder = "${env:DOWNLOAD_DIR}/plantuml"

    Write-Host "Creation du dossier."
    New-Item -ItemType Directory -Path "${env:PLANTUML_HOME}"

    Write-Host "Copy des fichier."

    Copy-Item -Path "${plantuml_folder}\*" -Destination "${env:PLANTUML_HOME}" -Recurse

    Set-Alias plantuml 'java -jar "${env:PLANTUML_HOME}\plantuml.jar"'
}