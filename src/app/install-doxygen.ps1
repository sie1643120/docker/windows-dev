function InstallDoxygen {
    $doxygen_folder = "${env:DOWNLOAD_DIR}/doxygen"

    # Obtenir tous les fichiers .msi dans le dossier
    $exeFiles = Get-ChildItem -Path "${doxygen_folder}" -Filter "doxygen*-setup.exe"

    # Si aucun fichier .exe n'est trouvé, arrêter le script
    if ($exeFiles.Count -eq 0) {
        throw "No Install of Doxygen found in download folder."
    }

    $doxygen_upper_version = $exeFiles | Sort-Object { Get-VersionFromFilename $_.Name } -Descending | Select-Object -First 1
    $doxygen_dist = "${doxygen_folder}/${doxygen_upper_version}"

    Write-Host "Installing git from ${doxygen_dist} into ${env:DOXYGEN_HOME}"
    $p = Start-Process -FilePath "${doxygen_dist}" `
        -ArgumentList ("/VERYSILENT", "/SUPPRESSMSGBOXES", "/ALLUSERS", "/NORESTART", "DIR ${env:DOXYGEN_HOME}") `
        -Wait -PassThru
    if (${p}.ExitCode -ne 0) {
        throw "Failed to install git"
    }
    Write-Host "Doxygen ${doxygen_upper_version} installed into ${env:DOXYGEN_HOME}"
}