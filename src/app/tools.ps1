
function Get-VersionFromFilename {
  param ($filename)
  # Regex pour capturer les versions dans le format X.X.X (ex : cmake-3.29.3-windows-x86_64)
  if ($filename -match "\d+(\.\d+)+") {
    return [Version]$matches[0]
  }
  return [Version]"0.0"
}