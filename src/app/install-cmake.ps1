function InstallCmake {

  $cmake_folder = "${env:DOWNLOAD_DIR}/cmake"


  # Obtenir tous les fichiers .msi dans le dossier
  $exeFiles = Get-ChildItem -Path "${cmake_folder}" -Filter *x86_64.msi
  # Si aucun fichier .exe n'est trouvé, arrêter le script
  if ($exeFiles.Count -eq 0) {
    throw "No Install of Cmake found in download folder."
  }


  function Get-CmakeVersionFromFilename {
    param ($filename)

    # Regex pour capturer les versions dans le format X.X.X (ex : cmake-3.29.3-windows-x86_64)
    if ($filename -match "\d+\.\d+\.\d+") {
      Write-Host $matches[0]
      return [Version]$matches[0]
    }
    return [Version]"0.0"
  }
  $cmake_upper_version = $exeFiles | Sort-Object { Get-VersionFromFilename $_.Name } -Descending | Select-Object -First 1
  $cmake_dist = "${cmake_folder}/${cmake_upper_version}"


  Write-Host "Installing Cmake from ${cmake_dist} into ${env:CMAKE_HOME}"
  $p = Start-Process -FilePath "${cmake_dist}" `
    -ArgumentList ("/norestart", "/quiet", "/qn", "ALLUSERS=1", "TargetDir=""${env:CMAKE_HOME}""") `
    -Wait -PassThru
  if (${p}.ExitCode -ne 0) {
    throw "Failed to install 7-Zip"
  }
  Write-Host "7-Zip ${cmake_upper_version} installed into ${env:CMAKE_HOME}"
}