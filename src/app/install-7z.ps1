function Install7zip {
  # Install 7-Zip
  $seven_zip_folder = "${env:DOWNLOAD_DIR}/7-zip"

  # Obtenir tous les fichiers .msi dans le dossier
  $exeFiles = Get-ChildItem -Path "${seven_zip_folder}" -Filter *x64.msi

  # Si aucun fichier .exe n'est trouvé, arrêter le script
  if ($exeFiles.Count -eq 0) {
    throw "No Install of 7zip found in download folder."
  }

  # Fonction pour extraire la version du nom de fichier
  function Get-7zipVersionFromFilename {
    param ($filename)

    # Regex pour capturer les versions dans le format X.X ou X.X.X (ex : 7z1900, 7z2201-x64)
    if ($filename -match "\d+(\.\d+)+") {
      return [Version]$matches[0]
    }
    return [Version]"0.0"
  }

  # Trouver le fichier avec la version la plus récente

  $seven_zip_upper_version = $exeFiles | Sort-Object { Get-VersionFromFilename $_.Name } -Descending | Select-Object -First 1
  $seven_zip_dist = "${seven_zip_folder}/${seven_zip_upper_version}"

  Write-Host "Installing 7-Zip from ${seven_zip_dist} into ${env:SEVEN_ZIP_HOME}"
  $p = Start-Process -FilePath "${seven_zip_dist}" `
    -ArgumentList ("/norestart", "/quiet", "/qn", "ALLUSERS=1", "TargetDir=""${env:SEVEN_ZIP_HOME}""") `
    -Wait -PassThru
  if (${p}.ExitCode -ne 0) {
    throw "Failed to install 7-Zip"
  }
  Write-Host "7-Zip ${seven_zip_upper_version} installed into ${env:SEVEN_ZIP_HOME}"

}